package com.springboot.crud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.crud.Entity.Employee;
import com.springboot.crud.Repository.EmployeeRepository;


@Service
public class EmployeeService {

	
	 @Autowired
	 private EmployeeRepository repo;
	 
	 public java.util.List<Employee> listAll() {
	        return repo.findAll();
	    }
	     
	    public void save(Employee std) {
	        repo.save(std);
	    }
	     
	    public Employee get(long id) {
	        return repo.findById(id).get();
	    }
	     
	    public void delete(long id) {
	        repo.deleteById(id);
	    }
	    
	 
}
