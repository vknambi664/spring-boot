package com.springboot.crud.Repository;

//2. Then create JPA repository
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.crud.Entity.Employee;


@Repository 
public interface EmployeeRepository  extends JpaRepository<Employee, Long> {

	
}
	

